<?php 

class Users extends Controller {
    public function __construct()
    {
        if(!isset($_SESSION["login"])) {
            return header('Location: ' . BASE_URL . '/auth/login');
        }
    }
    public function index($company = 'SMKN1')
    {   
        $data['title'] = "Users";
        $data['company'] = $company;
        $data['users'] = $this->model('User_model')->getAllUser();
        $this->view('templates/header', $data);
        $this->view('users/index', $data);
        $this->view('templates/footer');
    }

    public function create($company = 'SMKN1')
    {
        $data['title'] = "Users";
        $data['company'] = $company;
        $data['titleForm'] = 'Add User';
        $this->view('templates/header', $data);
        $this->view('users/form', $data);
        $this->view('templates/footer');
    }

    public function store()
    {
        if($this->model('User_model')->addUser($_POST) > 0) {
            Flasher::setFlash('User', 'Berhasil Ditambahkan!!', 'success');
            return header('Location: ' . BASE_URL . '/users');
            exit;
        } else {
            Flasher::setFlash('User', 'Gagal Ditambahkan!!', 'danger');
            return header('Location: ' . BASE_URL . '/create');
            exit;
        }
    }

    public function edit($id)
    {
        $data['title'] = "Users";
        $data['company'] = "SMKN 1";
        $data['users'] = $this->model('User_model')->getDetailUser($id);
        $data['titleForm'] = 'Edit User';
        $this->view('templates/header', $data);
        $this->view('users/form', $data);
        $this->view('templates/footer');
    }

    public function update()
    {
        if($this->model('User_model')->editUser($_POST) > 0) {
            Flasher::setFlash('User', 'Berhasil Diupdate!!', 'success');
            return header('Location: ' . BASE_URL . '/users');
            exit;
        } else {
            Flasher::setFlash('User', 'Berhasil Diupdate!!', 'danger');
            return header('Location: ' . BASE_URL . '/edit' . '/' . $_POST['id']);
            exit;
        }
    }

    public function delete($id)
    {
        if($this->model('User_model')->deleteUser($id) > 0) {
            Flasher::setFlash('User', 'Berhasil Dihapus!!', 'success');
            return header('Location: ' . BASE_URL . '/users');
            exit;
        } else {
            Flasher::setFlash('User', 'Gagal Dihapus!!', 'danger');
            return header('Location: ' . BASE_URL . '/');
            exit;
        }
    }
}