<?php 

class Home extends Controller{
    public function __construct()
    {
        if(!isset($_SESSION["login"])) {
            return header('Location: ' . BASE_URL . '/auth/login');
        }
    }
    
    public function index($company = 'SMKN1')
    {   
        $data['title'] = 'Home';
        $data['name'] = $this->model('User_model')->getUser();
        $data['users'] = $this->model('User_model')->getAllUser();
        $data['company'] = $company;
        $this->view('templates/header', $data);
        $this->view('home/index', $data);
        $this->view('templates/footer');
    }

    public function about($company = 'SMKN1')
    {
        $data['title'] = 'About';
        $data['name'] = 'Ryan Suranjana';
        $data['company'] = $company;
        $this->view('templates/header', $data);
        $this->view('about/index', $data);
        $this->view('templates/footer');
    }
}