<?php 

class User_model {
    private $table = 'users',
            $db,
            $name = 'Ryan';

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getUser()
    {
        return $_SESSION['auth']['username'];
    }

    public function getAllUser()
    {
        $this->db->query("SELECT * FROM {$this->table}");
        return $this->db->resultAll();
    }

    public function getUserById($id)
    {
        $this->db->query("SELECT * FROM {$this->table} WHERE id=:id");
        $this->db->bind('id', $id);
        return $this->db->resultSingle();
    }

    public function addUser($data)
    {
        $this->db->query("INSERT INTO {$this->table} (username, email, first_name, last_name, password) VALUES (:username, :email, :first_name, :last_name, :password)");
        
        $this->db->bind('username', htmlspecialchars($data['username']));
        $this->db->bind('email', htmlspecialchars($data['email']));
        $this->db->bind('first_name', htmlspecialchars($data['first_name']));
        $this->db->bind('last_name', htmlspecialchars($data['last_name']));
        $this->db->bind('password', htmlspecialchars(md5($data['password'] . SALT)));
        
        $this->db->execute();

        return $this->db->rowCount();
    }

    public function editUser($data)
    {
        $this->db->query("UPDATE {$this->table} SET username=:username, email=:email, first_name=:first_name, last_name=:last_name WHERE id=:id");

        $this->db->bind('username', htmlspecialchars($data['username']));
        $this->db->bind('email', htmlspecialchars($data['email']));
        $this->db->bind('first_name', htmlspecialchars($data['first_name']));
        $this->db->bind('last_name', htmlspecialchars($data['last_name']));
        $this->db->bind('id', htmlspecialchars($data['id']));

        $this->db->execute();

        $_SESSION['auth'] = $data;

        return $this->db->rowCount();
    }

    public function getDetailUser($id)
    {
        $this->db->query("SELECT * FROM {$this->table} WHERE id=:id");
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->resultSingle();
    }

    public function deleteUser($id)
    {
        $this->db->query("DELETE FROM {$this->table} WHERE id=:id");

        $this->db->bind('id', $id);

        $this->db->execute();

        return $this->db->rowCount();
    }

    public function register($data)
    {   
        $this->db->query("SELECT * FROM {$this->table} WHERE email=:email");

        $this->db->bind('email', $data['email']);

        $this->db->execute();
        $emailUnique = $this->db->resultSingle();

        if($emailUnique !== false) {
            return 0;
        } else if($data['password'] !== $data['confirm_password']) {
            return 0;
        } else {
            return $this->addUser($data);
        }
    }

    public function login($data)
    {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE email=:email AND password=:password');

        $this->db->bind('email', htmlspecialchars($data['email']));
        $this->db->bind('password', htmlspecialchars(md5($data['password'] . SALT)));

        $row = $this->db->resultSingle();

        if($row !== false) {
            $email_db = $row['email'];
            $password_db = $row['password'];
            if($data['email'] == $email_db && md5($data['password'] . SALT) == $password_db) {
                $_SESSION["login"] = true;
                $_SESSION["auth"] = $row;
                return true;
            }
        } else {
            return false;
        }
    }
}