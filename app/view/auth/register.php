<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
</head>
<body>
    <div class="card m-auto shadow mt-5" style="width: 30%;">
        <main class="form-control">
            <form method="post" action="<?= BASE_URL ?>/auth/registerPost">
                <h3 class="text-center my-3">Register</h3>
                <div class="form-floating">
                    <input type="text" class="form-control" id="username" placeholder="Username" name="username" required/>
                    <label for="username">Username</label>
                </div>
                <div class="form-floating">
                    <input type="text" class="form-control" id="first_name" placeholder="First Name" name="first_name" required/>
                    <label for="first_name">First Name</label>
                </div>
                <div class="form-floating">
                    <input type="text" class="form-control" id="last_name" placeholder="Last Name" name="last_name" required/>
                    <label for="last_name">Last Name</label>
                </div>
                <div class="form-floating">
                    <input type="email" class="form-control" id="email" placeholder="name@example.com" name="email" required/>
                    <label for="email">Email address</label>
                </div>
                <div class="form-floating">
                    <input type="password" class="form-control" id="password" placeholder="Password" name="password" required min="8"/>
                    <label for="password">Password</label>
                </div>
                <div class="form-floating">
                    <input type="password" class="form-control mb-3" id="confirm_password" placeholder="Konfirmasi Password" name="confirm_password" required min="8"/>
                    <label for="confirm_password">Confirm Password</label>
                </div>
                <button class="w-100 btn btn-lg btn-primary mb-3" type="submit" name="submit">Register</button>
                <div class="mb-3 w-100 text-center">
                    <p class="d-inline">have a account? </p><a href="<?= BASE_URL ?>/auth/login">Login</a>
                </div>
            </form>
        </main>
    </div>
    <script src="<?= BASE_URL ?>/assets/js/bootstrap.bundle.min.js"></script>
</body>
</html>