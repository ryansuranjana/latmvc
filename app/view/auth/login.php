<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
</head>
<body>
    <div class="card m-auto shadow mt-5" style="width: 30%;">
        <main class="form-signin p-3">
            <form method="POST" action="<?= BASE_URL ?>/auth/loginPost">
                <h1 class="h3 mt-3 mb-3 fw-normal text-center">Please sign in</h1>
                <div class="form-floating">
                    <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com" name="email" required/>
                    <label for="floatingInput">Email address</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="floatingPassword" placeholder="Password" name="password" required/>
                    <label for="floatingPassword">Password</label>
                </div>
                <div class="checkbox mb-3">
                    <label> <input type="checkbox" value="remember-me" name="remember"/> Remember me </label>
                </div>
                <button class="w-100 btn btn-lg btn-primary mb-3" type="submit" name="submit">Sign in</button>
                <div class="w-100 text-center">
                    <p class="pt-1 d-inline">Don't have a account? </p><a href="<?= BASE_URL ?>/auth/register">Register</a>
                </div>
            </form>
        </main>
    </div>
    <script src="<?= BASE_URL ?>/assets/js/bootstrap.bundle.min.js"></script>
</body>
</html>