<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <h3 class="my-4">Users</h3>
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5><?= $data['titleForm'] ?></h5>
                </div>
                <div class="card-body">
                <form method="POST" action="<?= isset($data['users']) ? BASE_URL . '/users/update' : BASE_URL . '/users/store' ?>">
                    <?php if(isset($data['users'])) { ?>
                        <input type="hidden" class="form-control" name="id" value="<?= $data['users']['id'] ?>" required>
                    <?php } ?>
                    <div class="mb-3">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" class="form-control" id="username" name="username" value="<?= isset($data['users']) ? $data['users']['username'] : '' ?>" required>
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" name="email" value="<?= isset($data['users']) ? $data['users']['email'] : '' ?>">
                    </div>
                    <?php if(!isset($data['users'])) { ?>
                        <div class="mb-3">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" class="form-control" id="password" name="password" required>
                        </div>
                    <?php } ?>
                    <div class="mb-3">
                        <label for="first_name" class="form-label">First Name</label>
                        <input type="text" class="form-control" id="first_name" name="first_name" value="<?= isset($data['users']) ? $data['users']['first_name'] : '' ?>" required>
                    </div>
                    <div class="mb-3">
                        <label for="last_name" class="form-label">Last Name</label>
                        <input type="text" class="form-control" id="last_name" name="last_name" value="<?= isset($data['users']) ? $data['users']['last_name'] : '' ?>" required>
                    </div>
                    <a href="<?= BASE_URL ?>/users" class="btn btn-secondary">Back</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                </div>
            </div>
        </div>
    </div>
</main>
