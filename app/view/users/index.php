<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <h3 class="my-4">Users</h3>
    <?php Flasher::flash(); ?>
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <h5>List Users</h5>
            <a href="<?= BASE_URL ?>/users/create" class="btn btn-success">Add</a>
        </div>
        <div class="card-body">
            <div class="mb-5 table">
                <table id="datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<script type="application/javascript">
    $(document).ready( function () {
        const myTable = $('#datatable').DataTable({
            "data": [],
            processing: true,
            orderable: true,
            autoWidth: false,
            order: [[0, "asc"]],
            "columnDefs": [
                { "width": "20%", "targets": 3 }
            ],
            "columns": [
                { data: "no", name: 'no', orderable: false },
                { data: "username", name: 'username', orderable: false },
                { data: "email", name: "email", orderable: false },
                { data: "action", name: "action", orderable: false },
            ]
        });
        const fetch = '<?= json_encode($data['users']) ?>'
        const data = JSON.parse(fetch)
        const logs = []
        $.each(data, function (index, value) {
            const row = {
                "no": ++index,
                "username" : value.username,
                "email" : value.email,
                "action": `<div class="flex">
                                <a href="<?= BASE_URL ?>/users/edit/${value.id}" class="btn btn-light border"><i class="bi bi-pencil-square"></i></a>
                                <a href="<?= BASE_URL ?>/users/delete/${value.id}" class="btn btn-danger" onclick="return confirm('apakah and yakin?')"><i class="bi bi-trash-fill"></i></a>
                            </div>`,
            }
            logs.push(row)
        })
        myTable.clear()
        $.each(logs, function(index, value) {
            myTable.row.add(value);
        })
        myTable.draw()
    } );
</script>